FROM ubuntu:20.04
RUN \
  apt update && \
  apt -y upgrade && \
  apt install -y make && \
  apt install -y git && \
  apt install -y build-essential
